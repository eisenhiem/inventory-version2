<?php

namespace app\controllers;

use app\models\Routine;
use app\models\RoutineSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RoutineController implements the CRUD actions for Routine model.
 */
class RoutineController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Routine models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new RoutineSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Routine model.
     * @param int $routine_id Routine ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($routine_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($routine_id),
        ]);
    }

    /**
     * Creates a new Routine model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($id)
    {
        $model = new Routine();
        $model->ITEM_ID = $id;
        $model->routine_date = date('Y-m-d');

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['items/view', 'id' => $model->ITEM_ID]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Routine model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $routine_id Routine ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($routine_id)
    {
        $model = $this->findModel($routine_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'routine_id' => $model->routine_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Routine model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $routine_id Routine ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($routine_id)
    {
        $this->findModel($routine_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Routine model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $routine_id Routine ID
     * @return Routine the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($routine_id)
    {
        if (($model = Routine::findOne(['routine_id' => $routine_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
