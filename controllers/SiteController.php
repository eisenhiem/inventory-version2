<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Items;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $server = Items::find()->where(['ITEM_TYPE_ID'=>5])->count();
        $desktop = Items::find()->where(['ITEM_TYPE_ID'=>1])->count();
        $laptop = Items::find()->where(['ITEM_TYPE_ID'=>3])->count();
        $ups = Items::find()->where(['ITEM_TYPE_ID'=>7])->count();
        $printer = Items::find()->where(['ITEM_TYPE_ID'=>8])->count();
        $software = Items::find()->where(['ITEM_TYPE_ID'=>9])->count();
        $farm = Items::find()->where(['ITEM_TYPE_ID'=>10])->count();
        $other_com = Items::find()->where(['ITEM_TYPE_ID'=>11])->count();
        $ads = Items::find()->where(['ITEM_TYPE_ID'=>12])->count();
        $home = Items::find()->where(['ITEM_TYPE_ID'=>13])->count();
        $med = Items::find()->where(['ITEM_TYPE_ID'=>15])->count();
        $electronic = Items::find()->where(['ITEM_TYPE_ID'=>16])->count();
        $vachine = Items::find()->where(['ITEM_TYPE_ID'=>17])->count();
        $office = Items::find()->where(['ITEM_TYPE_ID'=>18])->count();
        $sport = Items::find()->where(['ITEM_TYPE_ID'=>19])->count();
        $other = Items::find()->where(['ITEM_TYPE_ID'=>20])->count();
        $building = Items::find()->where(['ITEM_TYPE_ID'=>21])->count();
        $other_building = Items::find()->where(['ITEM_TYPE_ID'=>22])->count();
        $access_point = Items::find()->where(['ITEM_TYPE_ID'=>23])->count();

        return $this->render('index',[
            'server' => $server,
            'desktop' => $desktop,
            'laptop' => $laptop,
            'ups' => $ups,
            'printer' => $printer,
            'access_point' => $access_point,
            'software' => $software,
            'farm' => $farm,
            'other_com' => $other_com,
            'ads' => $ads,
            'home' => $home,
            'med' => $med,
            'electronic' => $electronic,
            'vachine' => $vachine,
            'office' => $office,
            'sport' => $sport,
            'other' => $other,
            'building' => $building,
            'other_building' => $other_building,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
