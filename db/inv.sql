/*
 Navicat Premium Data Transfer

 Source Server         : api
 Source Server Type    : MySQL
 Source Server Version : 100141
 Source Host           : 192.168.111.2:3306
 Source Schema         : inv

 Target Server Type    : MySQL
 Target Server Version : 100141
 File Encoding         : 65001

 Date: 23/02/2022 11:32:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for check
-- ----------------------------
DROP TABLE IF EXISTS `check`;
CREATE TABLE `check`  (
  `CHECK_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ITEM_ID` int(10) UNSIGNED NOT NULL,
  `CHECK_DATE` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `CHECK_RESULT_ID` tinyint(1) NULL DEFAULT NULL,
  `COMMENT` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `CHECK_BY_ID` tinyint(2) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`CHECK_ID`) USING BTREE,
  INDEX `item_id`(`ITEM_ID`) USING BTREE,
  INDEX `checkby`(`CHECK_BY_ID`) USING BTREE,
  CONSTRAINT `check_ibfk_2` FOREIGN KEY (`CHECK_BY_ID`) REFERENCES `check_by` (`CHECK_BY_ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `check_ibfk_3` FOREIGN KEY (`ITEM_ID`) REFERENCES `items` (`ITEM_ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for check_by
-- ----------------------------
DROP TABLE IF EXISTS `check_by`;
CREATE TABLE `check_by`  (
  `CHECK_BY_ID` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `CHECK_BY_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`CHECK_BY_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for check_result
-- ----------------------------
DROP TABLE IF EXISTS `check_result`;
CREATE TABLE `check_result`  (
  `CHECK_RESULT_ID` tinyint(1) NOT NULL AUTO_INCREMENT,
  `CHECK_RESULT_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`CHECK_RESULT_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `department_id` int(2) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(255) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL,
  `department_code` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`department_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for item_type
-- ----------------------------
DROP TABLE IF EXISTS `item_type`;
CREATE TABLE `item_type`  (
  `ITEM_TYPE_ID` tinyint(2) NOT NULL AUTO_INCREMENT,
  `ITEM_TYPE_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ITEM_TYPE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items`  (
  `ITEM_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ITEM_NO` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_TYPE_ID` tinyint(2) NULL DEFAULT NULL,
  `RECEIVE_DATE` date NULL DEFAULT NULL,
  `WARRANTY_EXPIRE` date NULL DEFAULT NULL,
  `PRODUCT_PRICE` double NULL DEFAULT NULL,
  `DEPRECIATION` double NULL DEFAULT NULL,
  `SOURCE_ID` int(11) NULL DEFAULT NULL,
  `LOCATION_ID` tinyint(2) UNSIGNED NULL DEFAULT NULL,
  `STATUS` tinyint(1) UNSIGNED NULL DEFAULT 1,
  `INCHARGE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PICTURE` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PHOTO_FILE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`ITEM_ID`) USING BTREE,
  INDEX `itemtype`(`ITEM_TYPE_ID`) USING BTREE,
  INDEX `location`(`LOCATION_ID`) USING BTREE,
  CONSTRAINT `items_ibfk_1` FOREIGN KEY (`ITEM_TYPE_ID`) REFERENCES `item_type` (`ITEM_TYPE_ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `items_ibfk_2` FOREIGN KEY (`LOCATION_ID`) REFERENCES `location` (`LOCATION_ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2269 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for location
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location`  (
  `LOCATION_ID` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `LOCATION_NAME` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`LOCATION_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for move
-- ----------------------------
DROP TABLE IF EXISTS `move`;
CREATE TABLE `move`  (
  `MOVE_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ITEM_ID` int(11) NULL DEFAULT NULL,
  `MOVE_FORM_ID` int(11) NULL DEFAULT NULL,
  `MOVE_TO_ID` int(11) NULL DEFAULT NULL,
  `MOVE_DATE` date NULL DEFAULT NULL,
  PRIMARY KEY (`MOVE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for profile
-- ----------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile`  (
  `user_id` int(11) NOT NULL,
  `department_id` int(11) NULL DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cid` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position_level` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for repair_dischart
-- ----------------------------
DROP TABLE IF EXISTS `repair_dischart`;
CREATE TABLE `repair_dischart`  (
  `REPAIR_DISCHART_ID` tinyint(1) NOT NULL AUTO_INCREMENT,
  `REPAIR_DISCHART_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`REPAIR_DISCHART_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for repair_status
-- ----------------------------
DROP TABLE IF EXISTS `repair_status`;
CREATE TABLE `repair_status`  (
  `REPAIR_STATUS_ID` tinyint(1) UNSIGNED NOT NULL AUTO_INCREMENT,
  `REPAIR_STATUS_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`REPAIR_STATUS_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for repair_type
-- ----------------------------
DROP TABLE IF EXISTS `repair_type`;
CREATE TABLE `repair_type`  (
  `REPAIR_TYPE_ID` tinyint(2) NOT NULL AUTO_INCREMENT,
  `REPAIR_TYPE_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`REPAIR_TYPE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for repairs
-- ----------------------------
DROP TABLE IF EXISTS `repairs`;
CREATE TABLE `repairs`  (
  `REPAIR_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ITEM_ID` int(10) UNSIGNED NOT NULL,
  `REQUIRE_DATE` date NOT NULL,
  `PROBLEM_CUASE` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `REPAIR_DATE` date NULL DEFAULT NULL,
  `QUARANTINE_DATE` date NULL DEFAULT NULL,
  `FINISH_DATE` date NULL DEFAULT NULL,
  `REPAIR_RESULT` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `REPAIR_TYPE_ID` tinyint(1) NULL DEFAULT NULL,
  `REPAIR_DISCHART_ID` tinyint(1) NULL DEFAULT NULL,
  `REQUIRE_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CHECK_BY_ID` tinyint(2) NULL DEFAULT NULL,
  `REPAIR_STATUS_ID` tinyint(1) NULL DEFAULT 1,
  `SIGN_ID` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`REPAIR_ID`) USING BTREE,
  INDEX `repair_type`(`REPAIR_TYPE_ID`) USING BTREE,
  INDEX `repair_dc`(`REPAIR_DISCHART_ID`) USING BTREE,
  INDEX `item`(`ITEM_ID`) USING BTREE,
  CONSTRAINT `repairs_ibfk_1` FOREIGN KEY (`ITEM_ID`) REFERENCES `items` (`ITEM_ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sign
-- ----------------------------
DROP TABLE IF EXISTS `sign`;
CREATE TABLE `sign`  (
  `ID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `DIRECTOR_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DIRECTOR_POSITION1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DIRECTOR_POSITION2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MANAGE_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MANAGE_POSITION` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IT_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IT_POSITION` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `HOSPITAL_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `HOSP_NO` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_ACTIVE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for source
-- ----------------------------
DROP TABLE IF EXISTS `source`;
CREATE TABLE `source`  (
  `SOURCE_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `SOURCE_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SOURCE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 393 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tmp_items
-- ----------------------------
DROP TABLE IF EXISTS `tmp_items`;
CREATE TABLE `tmp_items`  (
  `ITEM_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ITEM_NO` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_TYPE_ID` tinyint(2) NULL DEFAULT NULL,
  `RECEIVE_DATE` date NULL DEFAULT NULL,
  `WARRANTY_EXPIRE` date NULL DEFAULT NULL,
  `PRODUCT_PRICE` double NULL DEFAULT NULL,
  `DEPRECIATION` double NULL DEFAULT NULL,
  `SOURCE_ID` int(11) NULL DEFAULT NULL,
  `LOCATION_ID` tinyint(2) UNSIGNED NULL DEFAULT NULL,
  `STATUS` tinyint(1) UNSIGNED NULL DEFAULT 1,
  `INCHARGE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PICTURE` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PHOTO_FILE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`ITEM_ID`) USING BTREE,
  INDEX `itemtype`(`ITEM_TYPE_ID`) USING BTREE,
  INDEX `location`(`LOCATION_ID`) USING BTREE,
  CONSTRAINT `tmp_items_ibfk_1` FOREIGN KEY (`ITEM_TYPE_ID`) REFERENCES `item_type` (`ITEM_TYPE_ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `tmp_items_ibfk_2` FOREIGN KEY (`LOCATION_ID`) REFERENCES `location` (`LOCATION_ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1560 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tmp_loc
-- ----------------------------
DROP TABLE IF EXISTS `tmp_loc`;
CREATE TABLE `tmp_loc`  (
  `deccod` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `location_id` int(10) UNSIGNED NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token`  (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE INDEX `token_unique`(`user_id`, `code`, `type`) USING BTREE,
  CONSTRAINT `token_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) NULL DEFAULT NULL,
  `unconfirmed_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `blocked_at` int(11) NULL DEFAULT NULL,
  `registration_ip` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT 0,
  `last_login_at` int(11) NULL DEFAULT NULL,
  `role` int(2) NULL DEFAULT 2,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_unique_username`(`username`) USING BTREE,
  UNIQUE INDEX `user_unique_email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 99 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- View structure for list
-- ----------------------------
DROP VIEW IF EXISTS `list`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`%` SQL SECURITY DEFINER VIEW `list` AS select concat('หมายเลขครุภัณฑ์ : ',`i`.`ITEM_NO`) AS `serial`,concat('ชื่อครุภัณฑ์ : ',`i`.`ITEM_NAME`) AS `name`,concat('ที่ตั้ง : ',`l`.`LOCATION_NAME`) AS `location`,concat('วันที่รับ : ',date_format(`i`.`RECEIVE_DATE`,'%d/%m/'),(year(`i`.`RECEIVE_DATE`) + 543)) AS `rdate`,concat('แหล่งงบ : ',`s`.`SOURCE_NAME`) AS `s`,concat('http://192.168.111.3/inv/web/index.php?r=items%2Fview&id=',`i`.`ITEM_ID`) AS `path` from ((`items` `i` join `source` `s` on((`i`.`SOURCE_ID` = `s`.`SOURCE_ID`))) join `location` `l` on((`i`.`LOCATION_ID` = `l`.`LOCATION_ID`))) order by `l`.`LOCATION_ID`,`i`.`ITEM_NO`;

SET FOREIGN_KEY_CHECKS = 1;
