/*
 Navicat Premium Data Transfer

 Source Server         : api
 Source Server Type    : MySQL
 Source Server Version : 100141
 Source Host           : 192.168.111.2:3306
 Source Schema         : inv

 Target Server Type    : MySQL
 Target Server Version : 100141
 File Encoding         : 65001

 Date: 23/02/2022 11:33:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for item_type
-- ----------------------------
DROP TABLE IF EXISTS `item_type`;
CREATE TABLE `item_type`  (
  `ITEM_TYPE_ID` tinyint(2) NOT NULL AUTO_INCREMENT,
  `ITEM_TYPE_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ITEM_TYPE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of item_type
-- ----------------------------
INSERT INTO `item_type` VALUES (1, 'เครื่องคอมพิวเตอร์แบบตั้งโต๊ะ');
INSERT INTO `item_type` VALUES (2, 'ครุภัณฑ์การแพทย์');
INSERT INTO `item_type` VALUES (3, 'เครื่องคอมพิวเตอร์แบบพกพา (Notebook)');
INSERT INTO `item_type` VALUES (4, 'เครื่องคอมพิวเตอร์แบบจอสัมผัส(I-Pad)');
INSERT INTO `item_type` VALUES (5, 'เครื่องคอมพิวเตอร์แม่ข่าย (Server)');
INSERT INTO `item_type` VALUES (6, 'จอคอมพิวเตอร์');
INSERT INTO `item_type` VALUES (7, 'เครื่องสำรองไฟ');
INSERT INTO `item_type` VALUES (8, 'เครื่องพิมพ์เอกสาร');
INSERT INTO `item_type` VALUES (9, 'วัสดุคอมพิวเตอร์');
INSERT INTO `item_type` VALUES (10, 'ครุภัณฑ์การเกษตร');
INSERT INTO `item_type` VALUES (11, 'ครุภัณฑ์คอมพิวเตอร์');
INSERT INTO `item_type` VALUES (12, 'ครุภัณฑ์โฆษณา');
INSERT INTO `item_type` VALUES (13, 'ครุภัณฑ์งานบ้านงานครัว');
INSERT INTO `item_type` VALUES (15, 'ครุภัณฑ์วิทยาศาสตร์การแพทย์');
INSERT INTO `item_type` VALUES (16, 'ครุภัณฑ์ไฟฟ้าและวิทยุ');
INSERT INTO `item_type` VALUES (17, 'ครุภัณฑ์ยานภาหนะและขนส่ง');
INSERT INTO `item_type` VALUES (18, 'ครุภัณฑ์สำนักงาน');
INSERT INTO `item_type` VALUES (19, 'ครุภัณฑ์การกีฬา');
INSERT INTO `item_type` VALUES (20, 'ครุภัณฑ์อื่น');
INSERT INTO `item_type` VALUES (21, 'สิ่งปลูกสร้าง');
INSERT INTO `item_type` VALUES (22, 'อาคารเพื่อประโยชน์อื่น');
INSERT INTO `item_type` VALUES (23, 'อุปกรณ์กระจายสัญญาณ');

SET FOREIGN_KEY_CHECKS = 1;
