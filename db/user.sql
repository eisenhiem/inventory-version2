/*
 Navicat Premium Data Transfer

 Source Server         : api
 Source Server Type    : MySQL
 Source Server Version : 100141
 Source Host           : 192.168.111.2:3306
 Source Schema         : inv

 Target Server Type    : MySQL
 Target Server Version : 100141
 File Encoding         : 65001

 Date: 23/02/2022 11:39:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) NULL DEFAULT NULL,
  `unconfirmed_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `blocked_at` int(11) NULL DEFAULT NULL,
  `registration_ip` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT 0,
  `last_login_at` int(11) NULL DEFAULT NULL,
  `role` int(2) NULL DEFAULT 2,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_unique_username`(`username`) USING BTREE,
  UNIQUE INDEX `user_unique_email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 99 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'lsk.hospital@gmail.com', '$2y$12$dI98GVzJ5Zyv1EJDYRcAeeuH0hlYklXZI7KiNrPggLe7bWo5zdue6', 'bylfadG2nkgPJcywRIF3fCjTuKhvNPKU', 1628149204, NULL, NULL, '::1', 1615301772, 1615301772, 0, 1644826896, 1);
INSERT INTO `user` VALUES (98, 'u03807', 'u03807@u03807.com', '$2y$12$LHZ7wQZPwsIiG6gWUE/8NulX59Ax7F3URAE9hMch/1E.VCuTFroy2', '6ifzzmzdOVcM8UeThxia7DXC6pMTWkhT', 1636277692, NULL, NULL, '::1', 1636277692, 1636277692, 0, 1636421445, 2);

SET FOREIGN_KEY_CHECKS = 1;
