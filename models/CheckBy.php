<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "check_by".
 *
 * @property int $CHECK_BY_ID
 * @property string $CHECK_BY_NAME
 *
 * @property Check[] $checks
 */
class CheckBy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'check_by';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CHECK_BY_NAME'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CHECK_BY_ID' => 'รหัสผู้ตรวจสอบ',
            'CHECK_BY_NAME' => 'ผู้ตรวจสอบ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecks()
    {
        return $this->hasMany(Check::className(), ['CHECK_BY_ID' => 'CHECK_BY_ID']);
    }
}
