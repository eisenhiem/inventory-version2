<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CheckBy;

/**
 * CheckBySearch represents the model behind the search form of `app\models\CheckBy`.
 */
class CheckBySearch extends CheckBy
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CHECK_BY_ID'], 'integer'],
            [['CHECK_BY_NAME'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CheckBy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'CHECK_BY_ID' => $this->CHECK_BY_ID,
        ]);

        $query->andFilterWhere(['like', 'CHECK_BY_NAME', $this->CHECK_BY_NAME]);

        return $dataProvider;
    }
}
