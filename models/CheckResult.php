<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "check_result".
 *
 * @property int $CHECK_RESULT_ID
 * @property string $CHECK_RESULT_NAME
 *
 * @property Check[] $checks
 */
class CheckResult extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'check_result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CHECK_RESULT_ID'], 'required'],
            [['CHECK_RESULT_ID'], 'integer'],
            [['CHECK_RESULT_NAME'], 'string', 'max' => 255],
            [['CHECK_RESULT_ID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CHECK_RESULT_ID' => 'Check  Result  ID',
            'CHECK_RESULT_NAME' => 'ผลการตรวจสอบ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecks()
    {
        return $this->hasMany(Check::className(), ['CHECK_RESULT_ID' => 'CHECK_RESULT_ID']);
    }
}
