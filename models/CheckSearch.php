<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Check;

/**
 * CheckSearch represents the model behind the search form of `app\models\Check`.
 */
class CheckSearch extends Check
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CHECK_ID', 'ITEM_ID', 'CHECK_RESULT_ID', 'CHECK_BY_ID'], 'integer'],
            [['CHECK_DATE', 'COMMENT'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Check::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'CHECK_ID' => $this->CHECK_ID,
            'ITEM_ID' => $this->ITEM_ID,
            'CHECK_DATE' => $this->CHECK_DATE,
            'CHECK_RESULT_ID' => $this->CHECK_RESULT_ID,
            'CHECK_BY_ID' => $this->CHECK_BY_ID,
        ]);

        $query->andFilterWhere(['like', 'COMMENT', $this->COMMENT]);

        return $dataProvider;
    }
}
