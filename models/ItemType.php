<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_type".
 *
 * @property int $ITEM_TYPE_ID
 * @property string $ITEM_TYPE_NAME
 *
 * @property Items[] $items
 */
class ItemType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ITEM_TYPE_ID'], 'required'],
            [['ITEM_TYPE_ID'], 'integer'],
            [['ITEM_TYPE_NAME'], 'string', 'max' => 255],
            [['ITEM_TYPE_ID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ITEM_TYPE_ID' => 'Item  Type  ID',
            'ITEM_TYPE_NAME' => 'ประเภทอุปกรณ์',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['ITEM_TYPE_ID' => 'ITEM_TYPE_ID']);
    }
}
