<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ItemType;

/**
 * ItemTypeSearch represents the model behind the search form of `app\models\ItemType`.
 */
class ItemTypeSearch extends ItemType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ITEM_TYPE_ID'], 'integer'],
            [['ITEM_TYPE_NAME'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ItemType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ITEM_TYPE_ID' => $this->ITEM_TYPE_ID,
        ]);

        $query->andFilterWhere(['like', 'ITEM_TYPE_NAME', $this->ITEM_TYPE_NAME]);

        return $dataProvider;
    }
}
