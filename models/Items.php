<?php

namespace app\models;
use \yii\web\UploadedFile;

use Yii;
use DateTime;

/**
 * This is the model class for table "items".
 *
 * @property string $ITEM_ID
 * @property string $ITEM_NO
 * @property string $ITEM_NAME
 * @property int $ITEM_TYPE_ID
 * @property string $RECEIVE_DATE
 * @property string $WARRANTY_EXPIRE
 * @property double $PRODUCT_PRICE
 * @property double $DEPRECIATION
 * @property string $QRCODE
 * @property int $LOCATION_ID
 *
 * @property Check[] $checks
 * @property ItemType $itemtype
 * @property Location $location
 * @property Repairs[] $repairs
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items';
    }

    public $upload_folder ='uploads';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ITEM_TYPE_ID', 'LOCATION_ID', 'SOURCE_ID', 'STATUS'], 'integer'],
            [['RECEIVE_DATE', 'WARRANTY_EXPIRE'], 'safe'],
            [['PRODUCT_PRICE', 'DEPRECIATION'], 'number'],
            [['ITEM_NO'], 'string', 'max' => 30],
            [['ITEM_NAME'], 'string', 'max' => 50],
            [['INCHARGE'], 'string', 'max' => 100],
            [['PICTURE'], 'string', 'max' => 255],
            [['PHOTO_FILE'], 'file',
                'skipOnEmpty' => true,
                'extensions' => 'png,jpg,bmi'
            ],
            [['ITEM_TYPE_ID'], 'exist', 'skipOnError' => true, 'targetClass' => ItemType::className(), 'targetAttribute' => ['ITEM_TYPE_ID' => 'ITEM_TYPE_ID']],
            [['LOCATION_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['LOCATION_ID' => 'LOCATION_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ITEM_ID' => 'รหัสอุปกรณ์',
            'ITEM_NO' => 'เลขที่อุปกรณ์',
            'ITEM_NAME' => 'ชื่ออุปกรณ์',
            'ITEM_TYPE_ID' => 'ประเภทอุปกรณ์',
            'RECEIVE_DATE' => 'วันที่รับ',
            'WARRANTY_EXPIRE' => 'วันที่หมดประกัน',
            'PRODUCT_PRICE' => 'ราคา',
            'DEPRECIATION' => 'อายุการใช้งาน(ปี)',
            'INCHARGE' => 'ผู้รับผิดชอบ',
            'SOURCE_ID' => 'แหล่งงบ',
            'LOCATION_ID' => 'ที่ตั้ง',
            'PICTURE' => 'รูปภาพ',
            'STATUS' => 'สถานะ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecks()
    {
        return $this->hasMany(Check::className(), ['ITEM_ID' => 'ITEM_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemtype()
    {
        return $this->hasOne(ItemType::className(), ['ITEM_TYPE_ID' => 'ITEM_TYPE_ID']);
    }

    public function getItemtypeName(){
        $model=$this->itemtype;
        return $model?$model->ITEM_TYPE_NAME:'';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource(){
        return $this->hasOne(Source::className(), ['SOURCE_ID' => 'SOURCE_ID']);
    }

    public function getSourceName(){
        $model=$this->source;
        return $model?$model->SOURCE_NAME:'';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['LOCATION_ID' => 'LOCATION_ID']);
    }

    public function getLocationName(){
        $model=$this->location;
        return $model?$model->LOCATION_NAME:'';
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairs()
    {
        return $this->hasMany(Repairs::className(), ['ITEM_ID' => 'ITEM_ID']);
    }

    public function upload($model,$attribute)
    {
        $photo  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $photo !== null) {

            $fileName = md5($photo->baseName.time()) . '.' . $photo->extension;
        //$fileName = $photo->baseName . '.' . $photo->extension;
            if($photo->saveAs($path.$fileName)){
            return $fileName;
        } else { 
            return null;
        }
        
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUse()
    {
        $startdate = new DateTime($this->RECEIVE_DATE);
        $currentdate = new DateTime('Now');
        $interval = $startdate->diff($currentdate);
        $use_y = floor($interval->format('%R%a')/365.25);
        $use_m = ($interval->format('%R%a')/(365.25/12))%12;
        $use = $use_y." ปี ".$use_m." เดือน";
        return $use;
    }

    public function getUploadPath(){
        return Yii::getAlias('@webroot').'/'.$this->upload_folder.'/';
    }

    public function getUploadUrl(){
        return Yii::getAlias('@web').'/'.$this->upload_folder.'/';
    }

    public function getPhotoViewer(){
        return empty($this->PICTURE) ? Yii::getAlias('@web').'/img/none.png' : $this->getUploadUrl().$this->PICTURE;
    }
}
