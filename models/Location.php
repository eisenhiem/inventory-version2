<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "location".
 *
 * @property int $LOCATION_ID
 * @property string $LOCATION_NAME
 *
 * @property Items[] $items
 */
class Location extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LOCATION_NAME'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LOCATION_ID' => 'Location  ID',
            'LOCATION_NAME' => 'ห้อง/ฝ่าย/กลุ่มงาน',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['LOCATION_ID' => 'LOCATION_ID']);
    }
}
