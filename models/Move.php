<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "move".
 *
 * @property string $MOVE_ID
 * @property int $ITEM_ID
 * @property int $MOVE_FORM_ID
 * @property int $MOVE_TO_ID
 * @property string $MOVE_DATE
 */
class Move extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'move';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ITEM_ID', 'MOVE_FORM_ID', 'MOVE_TO_ID'], 'integer'],
            [['MOVE_DATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MOVE_ID' => 'Move ID',
            'ITEM_ID' => 'Item ID',
            'MOVE_FORM_ID' => 'Move Form ID',
            'MOVE_TO_ID' => 'Move To ID',
            'MOVE_DATE' => 'Move Date',
        ];
    }

    public function getOldlocation()
    {
        return $this->hasOne(Location::className(), ['LOCATION_ID' => 'MOVE_FORM_ID']);
    }

    public function getNewlocation()
    {
        return $this->hasOne(Location::className(), ['LOCATION_ID' => 'MOVE_TO_ID']);
    }

    public function getOldLocationName(){
        $model=$this->oldlocation;
        return $model?$model->LOCATION_NAME:'';
    }

    public function getNewLocationName(){
        $model=$this->newlocation;
        return $model?$model->LOCATION_NAME:'';
    }

}
