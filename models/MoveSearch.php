<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Move;

/**
 * MoveSearch represents the model behind the search form of `app\models\Move`.
 */
class MoveSearch extends Move
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MOVE_ID', 'ITEM_ID', 'MOVE_FORM_ID', 'MOVE_TO_ID'], 'integer'],
            [['MOVE_DATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Move::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'MOVE_ID' => $this->MOVE_ID,
            'ITEM_ID' => $this->ITEM_ID,
            'MOVE_FORM_ID' => $this->MOVE_FORM_ID,
            'MOVE_TO_ID' => $this->MOVE_TO_ID,
            'MOVE_DATE' => $this->MOVE_DATE,
        ]);

        return $dataProvider;
    }
}
