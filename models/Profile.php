<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property int $user_id
 * @property int|null $department_id
 * @property string|null $fullname
 * @property string|null $cid
 * @property string|null $position
 * @property string|null $position_level
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'department_id'], 'integer'],
            [['fullname'], 'string', 'max' => 255],
            [['cid'], 'string', 'max' => 13],
            [['position'], 'string', 'max' => 100],
            [['position_level'], 'string', 'max' => 30],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'department_id' => 'กลุ่มงาน',
            'fullname' => 'ชื่อ-สกุล',
            'cid' => 'เลขบัตรประชาชน',
            'position' => 'ตำแหน่ง',
            'position_level' => 'ระดับ',
        ];
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['department_id' => 'department_id']);
    }

    public function getDepartmentName($id){
        $dep = Department::find()->where(['department_id'=>$id])->one();
        return $dep->department_name ;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
