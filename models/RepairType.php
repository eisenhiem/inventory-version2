<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "repair_type".
 *
 * @property int $REPAIR_TYPE_ID
 * @property string $REPAIR_TYPE_NAME
 */
class RepairType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'repair_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['REPAIR_TYPE_ID'], 'required'],
            [['REPAIR_TYPE_ID'], 'integer'],
            [['REPAIR_TYPE_NAME'], 'string', 'max' => 255],
            [['REPAIR_TYPE_ID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'REPAIR_TYPE_ID' => 'Repair  Type  ID',
            'REPAIR_TYPE_NAME' => 'ประเภทการซ่อม',
        ];
    }
}
