<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "repairs".
 *
 * @property string $REPAIR_ID
 * @property string $ITEM_ID
 * @property string $REQUIRE_DATE
 * @property string $PROBLEM_CUASE
 * @property string $REPAIR_DATE
 * @property string $FINISH_DATE
 * @property string $REPAIR_RESULT
 * @property string $REPAIR_NAME
 * @property int $REPAIR_TYPE_ID
 * @property int $REPAIR_DISCHART_ID
 * @property int $SIGN_ID
 * @property string $REPAIR_MONITOR
 * @property Items $iTEM
 * @property RepairDischart $rEPAIRDISCHART
 * @property RepairType $rEPAIRTYPE
 */
class Repairs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'repairs';
    }

    public $upload_folder ='uploads';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ITEM_ID', 'REQUIRE_DATE'], 'required'],
            [['ITEM_ID', 'REPAIR_TYPE_ID', 'REPAIR_DISCHART_ID', 'CHECK_BY_ID','REPAIR_STATUS_ID','SIGN_ID'], 'integer'],
            [['REQUIRE_DATE', 'REPAIR_DATE','REQUIRE_NAME', 'FINISH_DATE','QUARANTINE_DATE'], 'safe'],
            [['PROBLEM_CUASE', 'REPAIR_RESULT','REQUIRE_NAME','PICTURE','COMMENT','REPAIR_MONITOR'], 'string'],
            [['ITEM_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['ITEM_ID' => 'ITEM_ID']],
            [['REPAIR_DISCHART_ID'], 'exist', 'skipOnError' => true, 'targetClass' => RepairDischart::className(), 'targetAttribute' => ['REPAIR_DISCHART_ID' => 'REPAIR_DISCHART_ID']],
            [['REPAIR_TYPE_ID'], 'exist', 'skipOnError' => true, 'targetClass' => RepairType::className(), 'targetAttribute' => ['REPAIR_TYPE_ID' => 'REPAIR_TYPE_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'REPAIR_ID' => 'Repair  ID',
            'ITEM_ID' => 'รหัสอุปกรณ์',
            'REQUIRE_DATE' => 'วันที่แจ้งซ่อม',
            'PROBLEM_CUASE' => 'ปัญหาที่พบ',
            'REPAIR_DATE' => 'วันที่ซ่อม',
            'FINISH_DATE' => 'วันที่ซ่อมเสร็จ',
            'QUARANTINE_DATE' => 'วันที่คาดว่าซ่อมเสร็จ',
            'REPAIR_RESULT' => 'ผลการซ่อม',
            'REPAIR_TYPE_ID' => 'ประเภทการซ่อม',
            'REPAIR_STATUS_ID' => 'สถานะการซ่อม',
            'REPAIR_DISCHART_ID' => 'การจำหน่าย',
            'REQUIRE_NAME' => 'ผู้แจ้ง',
            'CHECK_BY_ID' => 'ผู้ซ่อม',
            'PICTURE' => 'รูปประกอบ',
            'REPAIR_MONITOR' => 'สำหรับงานพัสดุ',
            'COMMENT' => 'หมายเหตุ',
            'SIGN_ID' => 'ผู้ลงนาม',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['ITEM_ID' => 'ITEM_ID']);
    }
    
    public function getItemNo(){
        $model=$this->item;
        return $model?$model->ITEM_NO:'';
    }

    public function getItemName(){
        $model=$this->item;
        return $model?$model->ITEM_NAME:'';
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDischart()
    {
        return $this->hasOne(RepairDischart::className(), ['REPAIR_DISCHART_ID' => 'REPAIR_DISCHART_ID']);
    }

    public function getDischartName(){
        $model=$this->dischart;
        return $model?$model->REPAIR_DISCHART_NAME:'';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairtype()
    {
        return $this->hasOne(RepairType::className(), ['REPAIR_TYPE_ID' => 'REPAIR_TYPE_ID']);
    }

    public function getRepairtypeName(){
        $model=$this->repairtype;
        return $model?$model->REPAIR_TYPE_NAME:'';
    }

    public function getRepairby()
    {
        return $this->hasOne(CheckBy::className(), ['CHECK_BY_ID' => 'CHECK_BY_ID']);
    }

    public function getRepairByName(){
        $model=$this->repairby;
        return $model?$model->CHECK_BY_NAME:'';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairstatus()
    {
        return $this->hasOne(RepairStatus::className(), ['REPAIR_STATUS_ID' => 'REPAIR_STATUS_ID']);
    }

    public function getRepairstatusName(){
        $model=$this->repairstatus;
        return $model?$model->REPAIR_STATUS_NAME:'';
    }

    
    public function upload($model,$attribute)
    {
        $photo  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $photo !== null) {

            $fileName = md5($photo->baseName.time()) . '.' . $photo->extension;
        //$fileName = $photo->baseName . '.' . $photo->extension;
            if($photo->saveAs($path.$fileName)){
            return $fileName;
        } else { 
            return null;
        }
        
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUploadPath(){
        return Yii::getAlias('@webroot').'/'.$this->upload_folder.'/';
    }

    public function getUploadUrl(){
        return Yii::getAlias('@web').'/'.$this->upload_folder.'/';
    }

    public function getPhotoViewer(){
        return empty($this->PICTURE) ? Yii::getAlias('@web').'/img/none.png' : $this->getUploadUrl().$this->PICTURE;
    }
}
