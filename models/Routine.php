<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "routine".
 *
 * @property int $routine_id
 * @property int $ITEM_ID
 * @property string $routine_date
 * @property string $detail
 * @property string|null $company_name
 * @property float|null $total
 * @property string|null $next_round
 * @property string|null $comment
 * @property string|null $d_update
 */
class Routine extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'routine';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['routine_date', 'ITEM_ID', 'detail'], 'required'],
            [['routine_date', 'next_round', 'd_update'], 'safe'],
            [['detail', 'comment'], 'string'],
            [['total'], 'number'],
            [['ITEM_ID'], 'integer'],
            [['company_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'routine_id' => 'Routine ID',
            'ITEM_ID' => 'Item ID',
            'routine_date' => 'วันที่ตรวจ',
            'detail' => 'รายการ',
            'company_name' => 'บริษัท/ผู้รับจ้าง/ร้านซ่อม',
            'total' => 'จำนวนเงิน',
            'next_round' => 'วันที่ตรวจรอบถัดไป',
            'comment' => 'หมายเหตุ',
            'd_update' => 'วันที่ปรับปรุงล่าสุด',
        ];
    }
}
