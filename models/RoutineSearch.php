<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Routine;

/**
 * RoutineSearch represents the model behind the search form of `app\models\Routine`.
 */
class RoutineSearch extends Routine
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['routine_id','ITEM_ID'], 'integer'],
            [['routine_date', 'detail', 'company_name', 'next_round', 'comment', 'd_update'], 'safe'],
            [['total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Routine::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'routine_id' => $this->routine_id,
            'ITEM_ID' => $this->ITEM_ID,
            'routine_date' => $this->routine_date,
            'total' => $this->total,
            'next_round' => $this->next_round,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'detail', $this->detail])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
