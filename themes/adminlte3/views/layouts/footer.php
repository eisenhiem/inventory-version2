<footer class="main-footer">
    <strong>Copyright &copy; 2018-2022 <a href="https://gitlab.com/eisenhiem/inventory-version2.git">Jakkrapong Wongkamalasai</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 2.0.0
    </div>
</footer>