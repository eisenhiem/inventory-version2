<?php

use kartik\icons\Icon;

Icon::map($this);

?>

<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <div style="text-align: center;background-color:#396EB0;">
        <a href="<?=\yii\helpers\Url::home() ?>" class="brand-link">
            <!-- <img src="@web/images/logo.jpg" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
            <span class="brand-text font-weight-bold" style="color: white;">INV MIS V.2</span>
        </a>
    </div>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <?php if (!Yii::$app->user->isGuest) {
            echo '<div class="user-panel mt-3 pb-3 mb-3 d-flex">';
            echo '<div class="image">';
            echo '<img src="' . $assetDir . '/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">';
            echo '</div>';
            echo '<div class="info">';
            echo '<a href="#" class="d-block">' . Yii::$app->user->identity->username . '</a>';
            echo '</div>';
            echo '</div>';
        } ?>

        <!-- SidebarSearch Form -->
        <!-- href be escaped -->
        <!-- <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <?php
            echo \hail812\adminlte\widgets\Menu::widget([
                'items' => [
                    ['label' => 'Dashboard', 'icon' => 'tachometer-alt', 'url' => ['/site/index']],
                    ['label' => 'ประเภทพัสดุ-ครุภัณฑ์', 'header' => true],
                    [
                        'label' => 'คอมพิวเตอร์', 'icon' => 'desktop', 'iconClassAdded' => 'text-danger',
                        'items' => [
                            [
                                'label' => 'Computer',
                                'icon' => 'desktop',
                                'items' => [
                                    ['label' => 'Server', 'url' => ['/items/list', 'type_id' => 5], 'icon' => 'server'],
                                    ['label' => 'Desktop', 'url' => ['/items/list', 'type_id' => 1], 'icon' => 'desktop'],
                                    ['label' => 'Laptop', 'url' => ['/items/list', 'type_id' => 3], 'icon' => 'laptop']
                                ]
                            ],
                            ['label' => 'Printer', 'url' => ['/items/list', 'type_id' => 8], 'icon' => 'print'],
                            ['label' => 'UPS', 'url' => ['/items/list', 'type_id' => 7], 'icon' => 'car-battery'],
                            ['label' => 'Access Point', 'url' => ['/items/list', 'type_id' => 23], 'icon' => 'wifi'],
                            ['label' => 'Software', 'url' => ['/items/list', 'type_id' => 9], 'icon' => 'code'],
                            ['label' => 'อื่นๆ', 'url' => ['/items/list', 'type_id' => 11], 'icon' => 'cubes'],
                        ]
                    ],
                    [
                        'label' => 'สำนักงาน', 'icon' => 'boxes',  'iconClassAdded' => 'text-primary',
                        'items' => [
                            ['label' => 'วิทยาศาสตร์การแพทย์', 'url' => ['/items/list', 'type_id' => 15], 'icon' => 'medkit'],
                            ['label' => 'สำนักงาน', 'url' => ['/items/list', 'type_id' => 18], 'icon' => 'box'],
                            ['label' => 'งานบ้านงานครัว', 'url' => ['/items/list', 'type_id' => 13], 'icon' => 'shopping-cart'],
                            ['label' => 'ยานภาหนะและขนส่ง', 'url' => ['/items/list', 'type_id' => 17], 'icon' => 'truck'],
                            ['label' => 'ไฟฟ้าและวิทยุ', 'url' => ['/items/list', 'type_id' => 16], 'icon' => 'bolt'],
                            ['label' => 'การเกษตร', 'url' => ['/items/list', 'type_id' => 10], 'icon' => 'leaf'],
                            ['label' => 'โฆษณา', 'url' => ['/items/list', 'type_id' => 12], 'icon' => 'camera'],
                            ['label' => 'การกีฬา', 'url' => ['/items/list', 'type_id' => 19], 'icon' => 'gamepad'],
                            ['label' => 'สิ่งปลูกสร้าง', 'url' => ['/items/list', 'type_id' => 21], 'icon' => 'building'],
                            ['label' => 'อาคารเพื่อประโยชน์อื่น', 'url' => ['/items/list', 'type_id' => 22], 'icon' => 'building'],
                            ['label' => 'ครุภัณฑ์อื่น', 'url' => ['/items/list', 'type_id' => 20], 'icon' => 'info'],
                        ]
                    ],
                    ['label' => 'Report','header' => true],
                    ['label' => 'รายการซ่อม', 'icon' => 'fa fa-wrench', 'url' => ['/repairs/admin']],
                    ['label' => 'รายการครุภัณฑ์เกินอายุงาน', 'icon' => 'fa fa-wrench', 'url' => ['/items/old']],
                    ['label' => 'Admin Menu', 'header' => true],
                    [
                        'label' => 'ตั้งค่า',
                        'icon' => 'cogs',
                        'badge' => '<span class="right badge badge-info">2</span>',
                        'visible' => Yii::$app->user->identity->role == 1,
                        'items' => [
                            ['label' => 'ตั้งค่าผู้ใช้', 'url' => ['/user/admin/index'], 'icon' => 'users',],
                            ['label' => 'รายการครุภัณฑ์', 'url' => ['/items/index'], 'icon' => 'boxes',],
                            ['label' => 'ประเภทอุปกรณ์', 'url' => ['/itemtype/index'], 'icon' => 'tag',],
                            ['label' => 'ที่มา', 'url' => ['/source/index'], 'icon' => 'file-invoice',],
                            ['label' => 'ประเภทการซ่อม', 'url' => ['/repairtype/index'], 'icon' => 'tools',],
                            ['label' => 'สถานที่ตั้งอุปกรณ์', 'url' => ['/location/index'], 'icon' => 'map-marker',],
                            ['label' => 'สถานะอุปกรณ์', 'url' => ['/repairstatus/index'], 'icon' => 'info-circle',],
                            ['label' => 'ผลการจำหน่าย', 'url' => ['/repairdischart/index'], 'icon' => 'dolly-flatbed',],
                            ['label' => 'ชนิดผลการตรวจ', 'url' => ['/checkresult/index'], 'icon' => 'check-square',],
                            ['label' => 'รายชื่อผู้ตรวจสอบ', 'url' => ['/checkby/index'], 'icon' => 'users-cog',],
                            ['label' => 'รายชื่อผู้ลงนาม', 'url' => ['/sign/index'], 'icon' => 'file-signature',],
                            ['label' => 'Gii',  'icon' => 'file-code', 'url' => ['/gii'], 'target' => '_blank', 'iconClassAdded' => 'text-warning'],
                        ]
                    ],
                    ['label' => 'ลงชื่อเข้าใช้', 'url' => ['/user/security/login'], 'icon' => 'sign-in-alt', 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Logout', 'url' => ['/user/security/logout'], 'icon' => 'sign-out-alt', 'visible' => !Yii::$app->user->isGuest, 'template' => '&emsp;<a href="{url}" data-method="post">{icon} ออกจากระบบ</a>'],
                    //['label' => 'Debug', 'icon' => 'bug', 'url' => ['/debug'], 'target' => '_blank'],
                    /*                    ['label' => 'MULTI LEVEL EXAMPLE', 'header' => true],
                    ['label' => 'Level1'],
                    [
                        'label' => 'Level1',
                        'items' => [
                            ['label' => 'Level2', 'iconStyle' => 'far'],
                            [
                                'label' => 'Level2',
                                'iconStyle' => 'far',
                                'items' => [
                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle'],
                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle'],
                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle']
                                ]
                            ],
                            ['label' => 'Level2', 'iconStyle' => 'far']
                        ]
                    ],
                    ['label' => 'Level1'],
                    ['label' => 'LABELS', 'header' => true],
                    ['label' => 'Important', 'iconStyle' => 'far', 'iconClassAdded' => 'text-danger'],
                    ['label' => 'Warning', 'iconClass' => 'nav-icon far fa-circle text-warning'],
                    ['label' => 'Informational', 'iconStyle' => 'far', 'iconClassAdded' => 'text-info'],
*/
                ],
            ]);
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>