<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\CheckResult;
use app\models\CheckBy;
use kartik\date\DatePicker;
use kartik\icons\Icon;

$chkresult = ArrayHelper::map(CheckResult::find()->all(), 'CHECK_RESULT_ID', 'CHECK_RESULT_NAME');
$chkby = ArrayHelper::map(CheckBy::find()->all(), 'CHECK_BY_ID', 'CHECK_BY_NAME');

/* @var $this yii\web\View */
/* @var $model app\models\Check */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card card-info text-center col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
    <?= $form->field($model, 'ITEM_ID')->hiddenInput(['value'=> $item_id,'maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'CHECK_RESULT_ID')->dropDownList($chkresult, ['prompt'=>'เลือกผลการตรวจสอบ'])  ?>

    <?= $form->field($model, 'COMMENT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'CHECK_BY_ID')->dropDownList($chkby, ['prompt'=>'เลือกผู้ตรวจสอบ']) ?>
    <br>
            <div class="form-group">
                <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
            </div>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
