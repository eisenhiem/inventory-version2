<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Check */

$this->title = 'เพิ่มรายการตรวจสอบ';
?>
<div class="check-create">

    <?= $this->render('_form', [
        'model' => $model,'item_id' => $item_id
    ]) ?>

</div>
