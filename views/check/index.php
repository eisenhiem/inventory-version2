<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CheckSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการตรวจสอบ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php // = Html::a('Create Check', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'heading' => "ทะเบียนการตรวจสอบ พัสดุ-ครุภัณฑ์ " . Html::a(
                Icon::show('plus'),
                ['create'],
                [
                    'class' => 'btn btn-warning',
                    'style' => [
                        'border-radius' => '20px'
                    ]
                ]
            ),
            'type' => GridView::TYPE_PRIMARY
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'ITEM_ID',
                'label'=>'เลขที่อุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getItemNo();
                }
            ],
            [
                'attribute'=>'ITEM_ID',
                'label'=>'ชื่ออุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getItemName();
                }
            ],
            'CHECK_DATE',
            //'CHECK_RESULT_ID',
            [
                'attribute'=>'CHECK_RESULT_ID',
                'label'=>'ผลการตรวจสอบ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getResultName();
                }
            ],
            [
                'attribute'=>'CHECK_BY_ID',
                'label'=>'ผู้ตรวจสอบ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getCheckByName();
                }
            ],
            'COMMENT:ntext',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
