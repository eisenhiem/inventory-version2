<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Check */

$this->title = 'ปรับปรุงรายการตรวจสอบ: ' . $model->CHECK_ID;

?>
<div class="check-update">

    <?= $this->render('_formupdate', [
        'model' => $model,
    ]) ?>

</div>
