<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CheckBy */

$this->title = 'เพิ่มผู้ตรวจสอบ';

?>
<div class="check-by-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
