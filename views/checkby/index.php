<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CheckBySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายชื่อผู้ตรวจสอบ';

?>
<div class="check-by-index">

    <div class="col-lg-6 offset-lg-3 col-md-10 offset-md-1">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'panel' => [
                'heading' => "รายชื่อผู้ตรวจสอบ พัสดุ-ครุภัณฑ์ " . Html::a(
                    Icon::show('plus'),
                    ['create'],
                    [
                        'class' => 'btn btn-warning',
                        'style' => [
                            'border-radius' => '20px'
                        ]
                    ]
                ),
                'type' => GridView::TYPE_PRIMARY
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'CHECK_BY_ID',
                'CHECK_BY_NAME',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '',
                    'options' => ['style' => 'width:110px;'],
                    'buttonOptions' => ['class' => 'btn btn-warning btn-sm'],
                    'template' => '{update}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a(Icon::show('edit') . ' แก้ไข', ['update', 'id' => $model->CHECK_BY_ID], ['class' => 'btn btn-warning', 'style' => ['width' => '100px']]);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>