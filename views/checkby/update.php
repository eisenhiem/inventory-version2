<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CheckBy */

$this->title = 'แก้ไขผู้ตรวจสอบ: ' . $model->CHECK_BY_NAME;

?>
<div class="check-by-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
