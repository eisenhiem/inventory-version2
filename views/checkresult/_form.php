<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CheckResult */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-result-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card card-info text-center col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
        <?= $form->field($model, 'CHECK_RESULT_NAME')->textInput(['maxlength' => true]) ?>
        <br>
        <div class="form-group">
            <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
