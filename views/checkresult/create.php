<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CheckResult */

$this->title = 'เพิ่มผลการตรวจสอบ';

?>
<div class="check-result-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
