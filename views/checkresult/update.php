<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CheckResult */

$this->title = 'แก้ไขผลการตรวจสอบ: ' . $model->CHECK_RESULT_ID;

?>
<div class="check-result-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
