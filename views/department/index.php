<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'กลุ่มงาน';

?>
<div class="department-index">
    <div class="col-lg-6 offset-lg-3 col-md-10 offset-md-1">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'panel' => [
                'heading' => "ทะเบียนสถานที่ตั้ง พัสดุ-ครุภัณฑ์ " . Html::a(
                    Icon::show('plus'),
                    ['create'],
                    [
                        'class' => 'btn btn-warning',
                        'style' => [
                            'border-radius' => '20px'
                        ]
                    ]
                ),
                'type' => GridView::TYPE_PRIMARY
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'department_id',
                'department_name',
                'department_code',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '',
                    'options' => ['style' => ['width' => '120px']],
                    'buttonOptions' => ['class' => 'btn btn-primary btn-sm'],
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a(Icon::show('edit'), ['update', 'department_id' => $model->department_id], ['class' => 'btn btn-success', 'style' => ['width' => '44px']]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a(Icon::show('trash'), ['delete', 'department_id' => $model->department_id], [
                                'class' => 'btn btn-danger',
                                'style' => ['width' => '44px'],
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]);
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>

</div>