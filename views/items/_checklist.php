<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

$this->title = 'ประวัติการตรวจอุปกรณ์: ' . $model->ITEM_NAME .' '. $model->ITEM_NO;
?>

<div class="items-index">
<h1><?= Html::encode($this->title) ?></h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'CHECK_DATE',
        [
            'attribute'=>'CHECK_RESULT_ID',
            'label'=>'ผลการตรวจสอบ',
            'format'=>'text',//raw, html
            'content'=>function($data){
                return $data->getResultName();
            }
        ],
        [
            'attribute'=>'CHECK_BY_ID',
            'label'=>'ผู้ตรวจสอบ',
            'format'=>'text',//raw, html
            'content'=>function($data){
                return $data->getCheckByName();
            }
        ],
        'COMMENT:ntext',
    ],
]); ?>
</div>