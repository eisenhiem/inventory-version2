<?php
use yii\helpers\Html;

function changeDate($date){
//ใช้ Function explode ในการแยกไฟล์ ออกเป็น  Array
  if(is_null($date)){
    return '...............................';
  } else {
    $get_date = explode("-",$date);
    //กำหนดชื่อเดือนใส่ตัวแปร $month
    $month = array("01"=>"มกราคม","02"=>"กุมภาพันธ์","03"=>"มีนาคม","04"=>"เมษายน","05"=>"พฤษภาคม","06"=>"มิถุนายน","07"=>"กรกฎาคม","08"=>"สิงหาคม","09"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
    //month
    $get_month = $get_date["1"];
    //year    
    $year = $get_date["0"]+543;
    return $get_date["2"]." ".$month[$get_month]." พ.ศ. ".$year;
  }
}

function checkRequire($data){
  if(is_null($data)){
    return '..............................................';
  } else {
    return $data;
  }
}
//การเรียกใช้งาน Function

?>
<div class="center">
  <?php echo $data->HOSPITAL_NAME ?>
</div> 
<div>
  <?php echo '<img src="'.$model->PHOTO_FILE.'" width=100>'?>
</div>
<div>
  ชื่อครุภัณฑ์ <?php echo $model->ITEM_NAME?>
</div>
<div>
  หมายเลขครุภัณฑ์ <?php echo $model->ITEM_NO ?>
</div>
<div>
  วันตรวจรับ <?php echo changeDate($model->RECEIVE_DATE) ?>
</div>
<div>
  ตำแหน่งที่ตั้ง <?php echo $model->getLocationName()?>
</div>
<div>
  ราคา <?php echo $model->PRODUCT_PRICE ?> บาท
</div>
<div>
  แหล่งงบ <?php echo $model->getSourceName() ?> 
<div>