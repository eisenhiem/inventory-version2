<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ItemType;
use app\models\Location;
use kartik\icons\Icon;

$locate = ArrayHelper::map(Location::find()->all(), 'LOCATION_ID', 'LOCATION_NAME');
$itemtype = ArrayHelper::map(ItemType::find()->all(), 'ITEM_TYPE_ID', 'ITEM_TYPE_NAME');
/* @var $this yii\web\View */
/* @var $model app\models\ItemsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['old'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-3 col-sm-4">
            <?= $form->field($model, 'ITEM_NAME')->textInput(['placeholder' => 'ชื่อพัสดุ-ครุภัณฑ์'])->label(false); ?>
        </div>
        <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'ITEM_TYPE_ID')->dropDownList($itemtype, ['prompt'=>'เลือกประเภทวัสดุ'])->label(false);  ?>
        </div>
        <div class="col-md-3 col-sm-4">
            <?= $form->field($model, 'LOCATION_ID')->dropDownList($locate, ['prompt' => 'เลือกตำแหน่งที่ตั้งวัสดุ'])->label(false); ?>
        </div>
        <div class="col-md-1 col-sm-2 form-group">
            <?= Html::submitButton(Icon::show('search'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
