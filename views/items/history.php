<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Items */

$this->title = 'ประวัติการซ่อม: ' . $model->ITEM_NO;

?>
<div class="items-index">

    <p>
    <?= Html::a('พิมพ์ประวัติการซ่อมบำรุง', ['printrepair','id' => $model->ITEM_ID], ['class' => 'btn btn-info']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'REPAIR_ID',
            // 'ITEM_ID',
            'REQUIRE_DATE',
            'PROBLEM_CUASE:ntext',
            'REPAIR_DATE',
            'FINISH_DATE',
            'REPAIR_RESULT:ntext',
            //'REPAIR_TYPE_ID',
            //'REPAIR_DISCHART_ID',
            [
                'attribute'=>'REPAIR_TYPE_ID',
                'label'=>'ประเภทอุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getRepairtypeName();
                }
            ],
            [
                'attribute'=>'REPAIR_DISCHART_ID',
                'label'=>'ผลการดำเนินการ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getDischartName();
                }
            ],

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
