<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\materialicons\MD;
use kartik\export\ExportMenu;
use kartik\icons\Icon;
use yii\bootstrap4\BootstrapAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการอุปกรณ์';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="items-index">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'heading' => "ทะเบียนรายการพัสดุ-ครุภัณฑ์ " . Html::a(Icon::show('plus'), ['create'], ['class' => 'btn btn-success']),
            'type' => GridView::TYPE_PRIMARY
        ],
        'exportConfig' => [],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ITEM_ID',
            'ITEM_NO',
            'ITEM_NAME',
            [
                'attribute' => 'ITEM_TYPE_ID',
                'label' => 'ประเภทอุปกรณ์',
                'format' => 'text', //raw, html
                'content' => function ($data) {
                    return $data->getItemtypeName();
                }
            ],
            [
                'attribute' => 'LOCATION_ID',
                'label' => 'ที่ตั้งอุปกรณ์',
                'format' => 'text', //raw, html
                'content' => function ($data) {
                    return $data->getLocationName();
                }
            ],
            'INCHARGE',
            //'RECEIVE_DATE',
            //'WARRANTY_EXPIRE',
            //'PRODUCT_PRICE',
            //'DEPRECIATION',
            //'QRCODE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>