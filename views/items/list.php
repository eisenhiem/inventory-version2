<?php

use kartik\icons\Icon;
use yii\helpers\Html;

$this->title = 'รายการ';
?>

<?= $this->render('_search_list', ['model' => $searchModel,'type_id' => $type_id]); ?>


<div class="row">
    <?php
    foreach ($model as $r) {
    ?>
        <div class="col-lg-3 col-md-6">
            <div class="card text-center">
                <div class="card-title bg-info" style="line-height: 50px;">เลขครุภัณฑ์ <?= $r->ITEM_NO ?></div>        
                <div class="card-body" style="background-color:#F0FFFF ;">
                <?= $model->PICTURE ? Html::img($model->photoViewer, ['class' => 'img-thumbnail', 'style' => 'width:300px;']) : Html::img(Yii::getAlias('@web') . '/images/no-image-found.png', ['class' => 'img-thumbnail', 'style' => 'width:300px;']) ?>                    <p class="card-text">
                        <?= Icon::show('hashtag') . $r->ITEM_NAME ?><br>
                        <?= Icon::show('thumbtack') .  $r->getLocationName(); ?><br>
                        <?= $r->INCHARGE ? Icon::show('user-tag') . $r->INCHARGE : Icon::show('user-tag') . 'ยังไม่ระบุ'; ?>
                    </p>
                </div>
                <div class="card-footer">
                <div class="row">
                    <div class="col-3">
                    <?= Html::a(Icon::show('tag') , ['view', 'id' => $r->ITEM_ID], ['class' => 'btn btn-primary btn-block']) ?>
                    </div>
                    <div class="col-3">
                    <?= Html::a(Icon::show('dolly') , ['move', 'id' => $r->ITEM_ID], ['class' => 'btn btn-info btn-block']) ?>
                    </div>
                    <div class="col-3">
                    <?= Html::a(Icon::show('screwdriver') , ['repair', 'id' => $r->ITEM_ID], ['class' => 'btn btn-warning btn-block']) ?>
                    </div>
                    <div class="col-3">
                    <?= Html::a(Icon::show('clipboard-check') , ['check', 'id' => $r->ITEM_ID], ['class' => 'btn btn-success btn-block']) ?>
                    </div>
                </div>

                </div>
            </div>
        </div>
    <?php } ?>
</div>