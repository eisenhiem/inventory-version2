<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MoveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ประวัติการเคลื่อนย้าย';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="move-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //= Html::a('Create Move', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'MOVE_ID',
            //'ITEM_ID',
            //'MOVE_FORM_ID',
            //'MOVE_TO_ID',
            [
                'attribute'=>'MOVE_FORM_ID',
                'label'=>'ย้ายจาก',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getOldLocationName();
                }
            ],
            [
                'attribute'=>'MOVE_TO_ID',
                'label'=>'ย้ายไปที่',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getNewLocationName();
                }
            ],

            'MOVE_DATE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
