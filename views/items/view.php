<?php

use app\models\Check;
use app\models\Move;
use app\models\Repairs;
use app\models\Routine;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

$move = Move::find()->where(['ITEM_ID' => $model->ITEM_ID])->all();
$repair = Repairs::find()->where(['ITEM_ID' => $model->ITEM_ID])->all();
$check = Check::find()->where(['ITEM_ID' => $model->ITEM_ID])->all();
$routine = Routine::find()->where(['ITEM_ID' => $model->ITEM_ID])->all();

/* @var $this yii\web\View */
/* @var $model app\models\Items */

$this->title = $model->ITEM_NO;

function changeDate($date){
    //ใช้ Function explode ในการแยกไฟล์ ออกเป็น  Array
      if(is_null($date)){
        return '...............................';
      } else {
        $get_date = explode("-",$date);
        //กำหนดชื่อเดือนใส่ตัวแปร $month
        $month = array("01"=>"มกราคม","02"=>"กุมภาพันธ์","03"=>"มีนาคม","04"=>"เมษายน","05"=>"พฤษภาคม","06"=>"มิถุนายน","07"=>"กรกฎาคม","08"=>"สิงหาคม","09"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
        //month
        $get_month = $get_date["1"];
        //year    
        $year = $get_date["0"]+543;
        return intVal($get_date["2"])." ".$month[$get_month]." พ.ศ. ".$year;
      }
    }
    
?>
<div class="items-view">
<?php 
/*
    <p>
    <div class="row">

        <div class="col-md-2">
            <?= Html::a('พิมพ์ Label', ['printlabel', 'id' => $model->ITEM_ID], ['class' => 'btn btn-info', 'style' => 'width:150px']) ?>
        </div>
        <div class="col-md-2">
            <?= Html::a('Generate QR Code', ['genqr', 'id' => $model->ITEM_ID], ['class' => 'btn btn-success', 'style' => 'width:150px']) ?>
        </div>
        <div class="col-md-2">
            <?= Html::a('ลบ', ['delete', 'id' => $model->ITEM_ID], [
                'class' => 'btn btn-danger',
                'style' => 'width:150px',
                'data' => [
                    'confirm' => 'คุณแน่ใจว่าต้องการลบรายการนี้ใช่หรือไม่?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
    </p>
*/ 
?>
    <div id="accordion">
        <div class="card">
            <div class="card-header bg-primary text-center" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="color:white;">
                        <?= Icon::show('tag') ?> ข้อมูลทั่วไป
                    </button>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <?= $model->PICTURE ? Html::img($model->photoViewer, ['class' => 'img-thumbnail', 'style' => 'width:300px;']) : Html::img(Yii::getAlias('@web') . '/images/no-image-found.png', ['class' => 'img-thumbnail', 'style' => 'width:300px;']) ?><br>
                    เลขครุภัณฑ์/เลขพัสดุ : <?= $model->ITEM_NO ?><br>
                    ชื่ออุปกรณ์ : <?= $model->ITEM_NAME ?><br>
                    ประเภทอุปกรณ์ : <?= $model->getItemtypeName(); ?> <br>
                    ที่ตั้งอุปกรณ์ : <?= $model->getLocationName(); ?><br>
                    ผู้รับผิดชอบ : <?= $model->INCHARGE ? $model->INCHARGE : 'ยังไม่ระบุ'; ?> <br>
                    วันที่รับ : <?= changeDate($model->RECEIVE_DATE); ?> <br>
                    ราคา : <?= number_format($model->PRODUCT_PRICE, 2); ?> บาท <br>
                    อายุการใช้งาน : <?= $model->DEPRECIATION ?> ปี (ใช้งานมาแล้ว <?= $model->getUse(); ?>) <br>
                    แหล่งงบ : <?= $model->getSourceName(); ?> <br>
                </div>
                <div class="card-footer">
                    <?= Yii::$app->user->identity->role == 1 ? Html::a(Icon::show('edit').' แก้ไข', ['update', 'id' => $model->ITEM_ID], ['class' => 'btn btn-primary btn-block']):'' ?>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header bg-info text-center" id="headingTwo">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="color:white;">
                    <?= Icon::show('clipboard-list') ?> ประวัติการย้าย
                    </button>
                </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                    <?php 
                        foreach($move as $m){
                            echo "<li> จาก ".$m->getOldLocationName().' ไปยัง '.$m->getNewLocationName().' เมื่อวันที่ '.changeDate($m->MOVE_DATE)."</li>";
                        } 
                    ?>
                    </ul>
                </div>
                <div class="card-footer">
                    <?= Yii::$app->user->identity->role == 1 ? Html::a(Icon::show('dolly').' ย้าย', ['move', 'id' => $model->ITEM_ID], ['class' => 'btn btn-info btn-block']):'' ?>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header bg-warning text-center" id="headingThree">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"  style="color:white;">
                        <?= Icon::show('toolbox') ?> ประวัติการแจ้งซ่อม
                    </button>
                </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    <?php 
                        foreach($repair as $r){
                            echo ' <h5 style="background-color:#FFF89A"> &emsp;'.changeDate($r->REQUIRE_DATE).'</h5>';
                            echo ' <ul class="list-group list-group-flush">';
                            echo ' <li class="list-group-item">ปัญหา : '.$r->PROBLEM_CUASE.'</li>';
                            echo ' <li class="list-group-item">ผู้แจ้ง : '.$r->REQUIRE_NAME.'</li>';
                            echo ' <li class="list-group-item">ผลการซ่อม : '.$r->REPAIR_RESULT.'</li>';
                            echo ' <li class="list-group-item">วันที่เสร็จ : '.changeDate($r->FINISH_DATE).'</li>';
                            echo ' <li class="list-group-item">ความเห็นงานพัสดุ : '.$r->REPAIR_MONITOR.'</li>';
                            echo ' <li class="list-group-item">หมายเหตุ : '.$r->COMMENT.'</li></ul>';
                            if(Yii::$app->user->identity->role == 1 && !$r->REPAIR_DISCHART_ID){
                                echo Html::a(Icon::show('screwdriver').'แก้ไขผลการซ่อม', ['/repairs/update', 'id' => $r->REPAIR_ID], ['class' => 'btn btn-danger btn-block']);
                            }
                            echo '<hr>';
                        } 
                    ?>
                </div>
                <div class="card-footer"><?= Html::a(Icon::show('screwdriver').' แจ้งซ่อม', ['repair', 'id' => $model->ITEM_ID], ['class' => 'btn btn-warning btn-block']) ?></div>
            </div>
        </div>
        <div class="card">
            <div class="card-header bg-success text-center" id="headingFour">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour" style="color:white;">
                    <?= Icon::show('calendar-check') ?> ประวัติการตรวจสอบ
                    </button>
                </h5>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                <div class="card-body">
                <?php 
                        foreach($check as $c){
                            echo ' <h5 style="background-color:#C1FFD7"> &emsp;'.changeDate(substr($c->CHECK_DATE,0,9)).'</h5>';
                            echo ' <ul class="list-group list-group-flush">';
                            echo ' <li class="list-group-item">ผลการตรวจสอบ : '.$c->getResultName().'</li>';
                            echo ' <li class="list-group-item">หมายเหตุ : '.$c->COMMENT.'</li>';
                            echo ' <li class="list-group-item">ผู้ตรวจสอบ : '.$c->getCheckByName().'</li></ul>';
                        } 
                    ?>                </div>
                <div class="card-footer"><?= Html::a(Icon::show('clipboard-check').' ตรวจสอบ', ['check', 'id' => $model->ITEM_ID], ['class' => 'btn btn-success btn-block']) ?></div>
            </div>
        </div>
        <div class="card">
            <div class="card-header bg-info text-center" id="headingFive">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive" style="color:white;">
                    <?= Icon::show('calendar-check') ?> ประวัติการซ่อมบำรุงรักษาทรัพย์สิน
                    </button>
                </h5>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                <div class="card-body">
                <?php 
                        foreach($routine as $r){
                            echo ' <h5 style="background-color:#BAD7E9"> &emsp;'.changeDate($r->routine_date).'</h5>';
                            echo ' <ul class="list-group list-group-flush">';
                            echo ' <li class="list-group-item">ผลการตรวจสอบ : '.$r->detail.'</li>';
                            echo ' <li class="list-group-item">บริษัท/ร้าน : '.$r->company_name.'</li>';
                            echo ' <li class="list-group-item">จำนวนเงิน : '.$r->total.'</li>';
                            echo ' <li class="list-group-item">รอบถัดไป : '.changeDate($r->next_round).'</li></ul>';
                        } 
                    ?>                </div>
                <div class="card-footer"><?= Html::a(Icon::show('clipboard-check').'บันทึกการซ่อมบำรุง', ['routine/create', 'id' => $model->ITEM_ID], ['class' => 'btn btn-info btn-block']) ?></div>
            </div>
        </div>
    </div>

</div>