<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ItemType */

$this->title = 'เพิ่มประเภทอุปกรณ์';

?>
<div class="item-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
