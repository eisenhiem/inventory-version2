<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ItemType */

$this->title = 'แก้ไขประเภทอุปกรณ์: ' . $model->ITEM_TYPE_ID;

?>
<div class="item-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
