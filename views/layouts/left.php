<?php

use kartik\sidenav\SideNav;
use yii\helpers\Url;

$item = [
    ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/']],
    ['label' => 'รายการ', 'icon' => 'ballot', 'url' => ['items/index']],
    // ['label' => 'Login', 'icon' => 'web', 'url' => ['site/login']],
    ['label' => 'รายงาน', 'icon' => 'bar_chart', 'url' => ['/javascript']],
    ['label' => 'ตั้งค่า', 'icon' => 'miscellaneous_services', 'visible' => !Yii::$app->user->isGuest, 'items' => [
        ['label' => 'เพิ่มผู้ใช้', 'icon' => 'person_add', 'url' => ['/user/admin/index']],
        ['label' => 'Error', 'icon' => 'text_format', 'url' => ['/error']],
        ['label' => 'Registration', 'icon' => 'text_format', 'url' => ['/registration']],
    ]],
];

?>

<?=
SideNav::widget([
    'type' => SideNav::TYPE_DEFAULT,
    'items' => $items,
    'heading' =>'Operations',
    'headingOptions' => ['class' => 'head-style']
])
?>