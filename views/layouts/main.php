<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use kartik\icons\Icon;
use kartik\sidenav\SideNav;

Icon::map($this);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>
    <div class="warpper">
        <left>
            <?=
            SideNav::widget([
                'type' => SideNav::TYPE_DEFAULT,
                'items' => [
                    ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/']],
                    ['label' => 'รายการ', 'icon' => 'ballot', 'url' => ['items/index']],
                    // ['label' => 'Login', 'icon' => 'web', 'url' => ['site/login']],
                    ['label' => 'รายงาน', 'icon' => 'bar_chart', 'url' => ['/javascript']],
                    ['label' => 'ตั้งค่า', 'icon' => 'miscellaneous_services', 'visible' => !Yii::$app->user->isGuest, 'items' => [
                        ['label' => 'เพิ่มผู้ใช้', 'icon' => 'person_add', 'url' => ['/user/admin/index']],
                        ['label' => 'Error', 'icon' => 'text_format', 'url' => ['/error']],
                        ['label' => 'Registration', 'icon' => 'text_format', 'url' => ['/registration']],
                    ]],
                ],
                'heading' => '',
                //'headingOptions' => ['class' => 'head-style'],
            ])
            ?>
        </left>
        <div class="main-panel">
            <header>
                <?php
                NavBar::begin([
                    'brandLabel' => Yii::$app->name,
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
                    ],
                ]);
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav'],
                    'items' => [
                        ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/']],
                        ['label' => 'รายการ', 'icon' => 'ballot', 'url' => ['items/index']],
                        // ['label' => 'Login', 'icon' => 'web', 'url' => ['site/login']],
                        ['label' => 'รายงาน', 'icon' => 'bar_chart', 'url' => ['/javascript']],
                        [
                            'label' => Icon::show('sliders-h') . 'ตั้งค่า',
                            'visible' => Yii::$app->user->isGuest ? false : Yii::$app->user->identity->role == 9, // check visible is admin
                            'items' => [
                                ['label' => 'ตั้งค่าข้อมูลผู้ใช้', 'url' => ['/profile/index']],
                                ['label' => 'ตั้งค่าผู้ใช้', 'url' => ['/user/admin/index']],
                            ]
                        ],

                        //    ['label' => 'Contact', 'url' => ['/site/contact']],
                        Yii::$app->user->isGuest ?
                            ['label' => Icon::show('sign-in-alt') . 'ลงชื่อเข้าระบบ', 'url' => ['/user/security/login']] :
                            ['label' => '(' . Yii::$app->user->identity->profile->fullname . ') ' . Icon::show('sign-out-alt') .  'ออกจากระบบ', 'url' => ['/user/security/logout'], 'linkOptions' => ['data-method' => 'post']],
                    ],
                ]);
                NavBar::end();
                ?>
            </header>

            <main role="main" class="flex-shrink-0">
                <div class="container">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= Alert::widget() ?>
                    <?= $content ?>
                </div>
            </main>

            <footer class="footer mt-auto py-3 text-muted">
                <div class="container">
                    <p class="float-left">&copy; My Company <?= date('Y') ?></p>
                    <p class="float-right"><?= Yii::powered() ?></p>
                </div>
            </footer>

        </div>
    </div>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>