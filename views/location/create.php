<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Location */

$this->title = 'เพิ่มสถานที่ตั้งอุปกรณ์';

?>
<div class="location-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
