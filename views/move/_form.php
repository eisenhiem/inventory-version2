<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Items;
use app\models\Location;
use kartik\date\DatePicker;
use kartik\icons\Icon;

$locate = ArrayHelper::map(Location::find()->all(), 'LOCATION_ID', 'LOCATION_NAME');

/* @var $this yii\web\View */
/* @var $model app\models\Move */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="move-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card card-info text-center col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
        <?= $form->field($model, 'MOVE_TO_ID')->dropDownList($locate, ['prompt' => 'เลือกตำแหน่งที่ตั้งวัสดุ']) ?>

        <?= $form->field($model, 'MOVE_DATE')->widget(
            DatePicker::ClassName(),
            [
                'name' => 'MOVE_DATE',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุวันที่ย้ายวัสดุ'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]
        ); ?> <br>
        <div class="form-group">
            <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>