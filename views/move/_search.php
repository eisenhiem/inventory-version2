<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MoveSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="move-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'MOVE_ID') ?>

    <?= $form->field($model, 'ITEM_ID') ?>

    <?= $form->field($model, 'MOVE_FORM_ID') ?>

    <?= $form->field($model, 'MOVE_TO_ID') ?>

    <?= $form->field($model, 'MOVE_DATE') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
