<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Move */

$this->title = 'Create Move';

?>
<div class="move-create">

    <?= $this->render('_form', [
        'model' => $model,'id' =>$id,'location'=>$location,
    ]) ?>

</div>
