<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MoveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ทะเบียนการย้ายสถานที่ตั้ง';

?>
<div class="move-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'heading' => "ทะเบียนการย้ายสถานที่ตั้ง พัสดุ-ครุภัณฑ์ " . Html::a(
                Icon::show('plus'),
                ['create'],
                [
                    'class' => 'btn btn-warning',
                    'style' => [
                        'border-radius' => '20px'
                    ]
                ]
            ),
            'type' => GridView::TYPE_PRIMARY
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'MOVE_ID',
            //'ITEM_ID',
            'MOVE_FORM_ID',
            'MOVE_TO_ID',
            'MOVE_DATE',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'options' => ['style' => 'width:110px;'],
                'buttonOptions' => ['class' => 'btn btn-warning btn-sm'],
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a(Icon::show('edit') . ' แก้ไข', ['update', 'id' => $model->MOVE_ID], ['class' => 'btn btn-warning', 'style' => ['width' => '100px']]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
