<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Move */

$this->title = 'Update Move: ' . $model->MOVE_ID;

?>
<div class="move-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
