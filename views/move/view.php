<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Move */

$this->title = $model->MOVE_ID;

\yii\web\YiiAsset::register($this);
?>
<div class="move-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->MOVE_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->MOVE_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'MOVE_ID',
            'ITEM_ID',
            'MOVE_FORM_ID',
            'MOVE_TO_ID',
            'MOVE_DATE',
        ],
    ]) ?>

</div>
