<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$role = [1=>'ผู้ใช้ทั่วไป',5=>'RM Man/หัวหน้ากลุ่ม/หัวหน้าฝ่าย',9=>'Admin/ประธาน/เลขา'];
/* @var $this yii\web\View */
/* @var $model app\models\Profile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'role')->radioList($role) ?>

    <div class="form-group">
        <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>