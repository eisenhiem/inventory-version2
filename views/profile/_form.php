<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Department;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;


$dep = ArrayHelper::map(Department::find()->all(), 'department_id', 'department_name');

/* @var $this yii\web\View */
/* @var $model app\models\Profile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profile-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="card text-right">
        <div class="card-body">
            <div class="col-md-8 offset-2 col-sm-10 offset-1">
                <div class="row">
                    <div class="col-md-3">
                        กลุ่มงาน :
                    </div>
                    <div class="col-md-9">
                        <?= $form->field($model, 'department_id')->dropDownList($dep)->label(false) ?>
                    </div>
                </div>

                <div class="row" style="vertical-align: middle;">
                    <div class="col-md-3">
                        ชื่อ-สกุล :
                    </div>
                    <div class="col-md-9">
                        <?= $form->field($model, 'fullname')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                </div>
                <div class="row" style="vertical-align: middle;">
                    <div class="col-md-3">
                        CID :
                    </div>
                    <div class="col-md-9">
                        <?= $form->field($model, 'cid')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                </div>
                <div class="row" style="vertical-align: middle;">
                    <div class="col-md-3">
                        ตำแหน่ง :
                    </div>
                    <div class="col-md-9">
                        <?= $form->field($model, 'position')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                </div>
                <div class="row" style="vertical-align: middle;">
                    <div class="col-md-3">
                        ระดับ :
                    </div>
                    <div class="col-md-9">
                        <?= $form->field($model, 'position_level')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>