<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RepairDischart */

$this->title = 'เพิ่มผลการจำหน่าย';

?>
<div class="repair-dischart-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
