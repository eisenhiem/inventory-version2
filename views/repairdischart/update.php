<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RepairDischart */

$this->title = 'แก้ไขผลการจำหน่าย: ' . $model->REPAIR_DISCHART_ID;

?>
<div class="repair-dischart-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
