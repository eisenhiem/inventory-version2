<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use app\models\RepairType;
use app\models\RepairDischart;
use app\models\CheckBy;
use kartik\icons\Icon;

$repairby = ArrayHelper::map(CheckBy::find()->all(), 'CHECK_BY_ID', 'CHECK_BY_ID');
$repairtype = ArrayHelper::map(RepairType::find()->all(), 'REPAIR_TYPE_ID', 'REPAIR_TYPE_NAME');
$dctype = ArrayHelper::map(RepairDischart::find()->all(), 'REPAIR_DISCHART_ID', 'REPAIR_DISCHART_NAME');

/* @var $this yii\web\View */
/* @var $model app\models\repairs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="repairs-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card card-info text-center col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
        <?= $form->field($model, 'ITEM_ID')->hiddenInput(['value' => $item_id, 'maxlength' => true])->label(false) ?>

        <?= $form->field($model, 'REQUIRE_DATE')->widget(
            DatePicker::ClassName(),
            [
                'name' => 'REQUIRE_DATE',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุวันที่แจ้งซ่อม'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]
        ); ?>

        <?= $form->field($model, 'PICTURE')->fileInput() ?>

        <?= $form->field($model, 'PROBLEM_CUASE')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'REQUIRE_NAME')->textInput(['maxlength' => true]) ?>
        <br>
        <div class="form-group">
            <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>