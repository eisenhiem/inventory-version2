<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\RepairType;
use app\models\RepairDischart;
use app\models\CheckBy;
use kartik\icons\Icon;

$repairby = ArrayHelper::map(CheckBy::find()->all(), 'CHECK_BY_ID', 'CHECK_BY_ID');
$repairtype = ArrayHelper::map(RepairType::find()->all(), 'REPAIR_TYPE_ID', 'REPAIR_TYPE_NAME');
$dctype = ArrayHelper::map(RepairDischart::find()->all(), 'REPAIR_DISCHART_ID', 'REPAIR_DISCHART_NAME');

/* @var $this yii\web\View */
/* @var $model app\models\repairs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="repairs-form">

    <div class="card card-info text-center col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
        <b>วันที่</b> <?= $model->REQUIRE_DATE ?><br>
        <b>ปัญหา</b> <?= $model->PROBLEM_CUASE ?><br>
        <b>ผู้แจ้ง</b> <?= $model->REQUIRE_NAME ?><br>
    </div>
    <?php $form = ActiveForm::begin(); ?>

    <div class="card card-info text-center col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">

        <?= $form->field($model, 'REPAIR_MONITOR')->textarea(['rows' => 3]) ?>

        <?= $form->field($model, 'COMMENT')->textarea(['rows' => 3]) ?>
        <br>
        <div class="form-group">
            <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>