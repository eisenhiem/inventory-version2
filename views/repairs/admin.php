<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\repairsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการซ่อม';
?>
<div class="repairs-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'heading' => "ทะเบียนการซ่อม พัสดุ-ครุภัณฑ์ ",
            'type' => GridView::TYPE_PRIMARY
        ],
        'rowOptions' => function ($model) {
            
            if (!$model->REPAIR_DISCHART_ID) {
                return ['style' => 'background-color:#EF9F9F;'];
            }
            else {
                return ['style' => 'background-color:#BDF2D5;'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'REPAIR_ID',
            //'ITEM_ID',
            [
                'attribute'=>'ITEM_ID',
                'label'=>'เลขที่อุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getItemNo();
                }
            ],
            [
                'attribute'=>'ITEM_ID',
                'label'=>'ชื่ออุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getItemName();
                }
            ],
            'REQUIRE_DATE',
            'QUARANTINE_DATE',
            'PROBLEM_CUASE:ntext',
//            'REQUIRE_NAME',
            [
                'attribute'=>'REPAIR_STATUS_ID',
                'label'=>'สถานะอุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getRepairStatusName();
                }
            ],
            'REPAIR_RESULT:ntext',
            [
                'attribute'=>'REPAIR_MONITOR',
                'label'=>'พัสดุ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->REPAIR_MONITOR;
                }
            ],
            [
                'attribute'=>'REPAIR_DISCHART_ID',
                'label'=>'ผลการจำหน่าย',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getDischartName();
                }
            ],
            'FINISH_DATE',
            //'REPAIR_TYPE_ID',
            //'REPAIR_DISCHART_ID',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'options' => ['style' => 'width:160px;'],
                'buttonOptions' => ['class' => 'btn btn-warning btn-sm'],
                'template' => '{print} {pastdu} {update}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Yii::$app->user->identity->role == 1 ? Html::a(Icon::show('edit'), ['update', 'id' => $model->REPAIR_ID], ['class' => 'btn btn-warning',]):'';
                    },
                    'pastdu' => function ($url, $model, $key) {
                        return in_array(Yii::$app->user->identity->role,[3,1]) ? Html::a(Icon::show('box'), ['patsdu', 'id' => $model->REPAIR_ID], ['class' => 'btn btn-success']):'';
                    },
                    'print' => function ($url, $model, $key) {
                        return Html::a(Icon::show('print'), ['pdf', 'id' => $model->REPAIR_ID], ['class' => 'btn btn-info', 'linkOptions' => ['target' => '_blank']]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
