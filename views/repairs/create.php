<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\repairs */

$this->title = 'เพิ่มรายการซ่อม';

?>
<div class="repairs-create">

    <?= $this->render('_form', [
        'model' => $model,'item_id' => $item_id
    ]) ?>

</div>
