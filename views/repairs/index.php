<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\repairsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการซ่อม';
?>
<div class="repairs-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'heading' => "ทะเบียนการซ่อม พัสดุ-ครุภัณฑ์ " . Html::a(
                Icon::show('plus'),
                ['create'],
                [
                    'class' => 'btn btn-warning',
                    'style' => [
                        'border-radius' => '20px'
                    ]
                ]
            ),
            'type' => GridView::TYPE_PRIMARY
        ],
        'panel' => [
            'before' => ''
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'ITEM_ID',
                'label'=>'เลขที่อุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getItemNo();
                }
            ],
            [
                'attribute'=>'ITEM_ID',
                'label'=>'ชื่ออุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getItemName();
                }
            ],
            'REQUIRE_DATE',
            'PROBLEM_CUASE:ntext',
            'REQUIRE_NAME',
            'QUARANTINE_DATE',
            'REPAIR_RESULT:ntext',
            [
                'attribute'=>'REPAIR_STATUS_ID',
                'label'=>'สถานะอุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getRepairStatusName();
                }
            ],
            [
                'attribute'=>'REPAIR_DISCHART_ID',
                'label'=>'ผลการดำเนินการ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getDischartName();
                }
            ],

        ],
    ]); ?>
</div>
