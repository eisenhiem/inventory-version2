<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\repairs */

$this->title = 'พัสดุบันทึก';

?>
<div class="repairs-patsdu">

    <?= $this->render('_form_patsdu', [
        'model' => $model,'item_id' => $item_id
    ]) ?>

</div>
