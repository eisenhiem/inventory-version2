<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\repairs */

$this->title = 'แก้ไขรายการซ่อม: ' . $model->REPAIR_ID;
?>
<div class="repairs-update">

    <?= $this->render('_formupdate', [
        'model' => $model,
    ]) ?>

</div>
