<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RepairStatus */

$this->title = 'เพิ่มสถานะการส่งซ่อม';

?>
<div class="repair-status-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
