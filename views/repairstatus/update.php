<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RepairStatus */

$this->title = 'Update Repair Status: ' . $model->REPAIR_STATUS_ID;

?>
<div class="repair-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
