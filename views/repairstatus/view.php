<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RepairStatus */

$this->title = $model->REPAIR_STATUS_ID;

\yii\web\YiiAsset::register($this);
?>
<div class="repair-status-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->REPAIR_STATUS_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->REPAIR_STATUS_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'REPAIR_STATUS_ID',
            'REPAIR_STATUS_NAME',
        ],
    ]) ?>

</div>
