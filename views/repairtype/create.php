<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RepairType */

$this->title = 'เพิ่มประเภทการซ่อม';

?>
<div class="repair-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
