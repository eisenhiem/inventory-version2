<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RepairType */

$this->title = 'แก้ไขประเภทการซ่อม: ' . $model->REPAIR_TYPE_ID;

?>
<div class="repair-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
