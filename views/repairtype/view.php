<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RepairType */

$this->title = $model->REPAIR_TYPE_ID;

?>
<div class="repair-type-view">

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->REPAIR_TYPE_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->REPAIR_TYPE_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณแน่ใจว่าต้องการลบรายการนี้ใช่หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'REPAIR_TYPE_ID',
            'REPAIR_TYPE_NAME',
        ],
    ]) ?>

</div>
