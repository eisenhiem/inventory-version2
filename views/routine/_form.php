<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\Routine */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="routine-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card card-info text-center col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
    <?= $form->field($model, 'routine_date')->widget(
            DatePicker::ClassName(),
            [
                'name' => 'routine_date',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุวันที่ตรวจ'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]
        ); ?>

    <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'next_round')->widget(
            DatePicker::ClassName(),
            [
                'name' => 'next_round',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุวันที่ตรวจรอบถัดไป'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]
        ); ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
    <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
