<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\RoutineSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="routine-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'routine_id') ?>

    <?= $form->field($model, 'routine_date')->widget(
            DatePicker::ClassName(),
            [
                'name' => 'routine_date',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุวันที่รับวัสดุ'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]
        ); ?>
        
    <?= $form->field($model, 'detail') ?>

    <?= $form->field($model, 'company_name') ?>

    <?= $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'next_round') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'd_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
