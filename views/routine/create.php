<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Routine */

$this->title = 'เพิ่มรายการตรวจซ่อมบำรุง';

?>
<div class="routine-create">
    <h3 align="center">รายการตรวจซ่อมบำรุงประจำรอบ</h3>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
