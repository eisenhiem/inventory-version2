<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoutineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Routines';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="routine-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Routine', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'routine_id',
            'routine_date',
            'detail:ntext',
            'company_name',
            'total',
            //'next_round',
            //'comment:ntext',
            //'d_update'
        ],
    ]); ?>


</div>
