<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Routine */

$this->title = 'Update Routine: ' . $model->routine_id;
$this->params['breadcrumbs'][] = ['label' => 'Routines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->routine_id, 'url' => ['view', 'routine_id' => $model->routine_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="routine-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
