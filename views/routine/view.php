<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Routine */

$this->title = $model->routine_id;
$this->params['breadcrumbs'][] = ['label' => 'Routines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="routine-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'routine_id' => $model->routine_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'routine_id' => $model->routine_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'routine_id',
            'routine_date',
            'detail:ntext',
            'company_name',
            'total',
            'next_round',
            'comment:ntext',
            'd_update',
        ],
    ]) ?>

</div>
