<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sign */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sign-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card card-info text-center col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
    <?= $form->field($model, 'HOSPITAL_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'HOSP_NO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DIRECTOR_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DIRECTOR_POSITION1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DIRECTOR_POSITION2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MANAGE_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MANAGE_POSITION')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'IT_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'IT_POSITION')->textInput(['maxlength' => true]) ?>
        <br>
        <div class="form-group">
            <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
