<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SignSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sign-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'DIRECTOR_NAME') ?>

    <?= $form->field($model, 'DIRECTOR_POSITION1') ?>

    <?= $form->field($model, 'DIRECTOR_POSITION2') ?>

    <?= $form->field($model, 'MANAGE_NAME') ?>

    <?php // echo $form->field($model, 'MANAGE_POSITION') ?>

    <?php // echo $form->field($model, 'IT_NAME') ?>

    <?php // echo $form->field($model, 'IT_POSITION') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
