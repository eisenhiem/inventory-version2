<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Sign */

$this->title = 'Create Sign';

?>
<div class="sign-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
