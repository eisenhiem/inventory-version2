<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\models\SignSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ผู้ลงนาม';

?>
<div class="sign-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'heading' => "ผู้ลงนาม พัสดุ-ครุภัณฑ์ " . Html::a(
                Icon::show('plus'),
                ['create'],
                [
                    'class' => 'btn btn-warning',
                    'style' => [
                        'border-radius' => '20px'
                    ]
                ]
            ),
            'type' => GridView::TYPE_PRIMARY
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ID',
            'HOSPITAL_NAME',
            'DIRECTOR_NAME',
            //'DIRECTOR_POSITION1',
            //'DIRECTOR_POSITION2',
            'MANAGE_NAME',
            //'MANAGE_POSITION',
            'IT_NAME',
            //'IT_POSITION',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'options' => ['style' => 'width:110px;'],
                'buttonOptions' => ['class' => 'btn btn-warning btn-sm'],
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a(Icon::show('edit') . ' แก้ไข', ['update', 'id' => $model->ID], ['class' => 'btn btn-warning', 'style' => ['width' => '100px']]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
