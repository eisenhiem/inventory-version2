<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sign */

$this->title = 'แก้ไขผู้ลงนาม: ' . $model->ID;

?>
<div class="sign-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
