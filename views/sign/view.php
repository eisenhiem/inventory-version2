<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sign */

$this->title = $model->ID;

?>
<div class="sign-view">


    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณแน่ใจว่าต้องการลบรายการนี้ใช่หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'ID',
            'HOSPITAL_NAME',
            'HOSP_NO',
            'DIRECTOR_NAME',
            'DIRECTOR_POSITION1',
            'DIRECTOR_POSITION2',
            'MANAGE_NAME',
            'MANAGE_POSITION',
            'IT_NAME',
            'IT_POSITION',
        ],
    ]) ?>

</div>
