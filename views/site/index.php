<?php
$this->title = 'Dashboard';
$this->params['breadcrumbs'] = [['label' => $this->title]];
?>
<div class="container-fluid">

    <div class="row">
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'Desktop/All-in-One',
                'number' => $desktop,
                'theme' => 'gradient-primary',
                'icon' => 'fa fa-desktop',
            ]) ?>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'Laptop/Notebook',
                'number' => $laptop,
                'theme' => 'gradient-info',
                'icon' => 'fa fa-laptop',
            ]) ?>
        </div>        
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'Printer/Scanner',
                'number' => $printer,
                'theme' => 'gradient-success',
                'icon' => 'fa fa-print',
            ]) ?>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'Server',
                'number' => $server,
                'theme' => 'gradient-secondary',
                'icon' => 'fa fa-server',
            ]) ?>
        </div>        
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'Access Point',
                'number' => $access_point,
                'theme' => 'gradient-warning',
                'icon' => 'fa fa-wifi',
            ]) ?>
        </div>        
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'UPS',
                'number' => $ups,
                'theme' => 'gradient-danger',
                'icon' => 'fa fa-car-battery',
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'อุปกรณ์การแพทย์',
                'number' => $med,
                'theme' => 'gradient-primary',
                'icon' => 'fa fa-medkit',
            ]) ?>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'สำนักงาน',
                'number' => $office,
                'theme' => 'gradient-info',
                'icon' => 'fa fa-box',
            ]) ?>
        </div>        
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'งานบ้านงานครัว',
                'number' => $home,
                'theme' => 'gradient-success',
                'icon' => 'fa fa-shopping-cart',
            ]) ?>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'การเกษตร',
                'number' => $farm,
                'theme' => 'gradient-secondary',
                'icon' => 'fa fa-leaf',
            ]) ?>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'ไฟฟ้า',
                'number' => $electronic,
                'theme' => 'gradient-warning',
                'icon' => 'fa fa-bolt',
            ]) ?>
        </div>        
        <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'ยานพาหนะ',
                'number' => $vachine,
                'theme' => 'gradient-danger',
                'icon' => 'fa fa-truck',
            ]) ?>
        </div>     
    </div>

    <div class="row">
        <div class="col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'Progress Rate',
                'number' => '41,410',
                'icon' => 'fa fa-wrench',
                'progress' => [
                    'width' => '70%',
                    'description' => '70% Increase in 30 Days'
                ]
            ]) ?>
        </div>
        <div class="col-md-6 col-12">
            <?= \hail812\adminlte\widgets\InfoBox::widget([
                'text' => 'Progress Rate',
                'number' => '41,410',
                'icon' => 'fa fa-dolly',
                'progress' => [
                    'width' => '70%',
                    'description' => '70% Increase in 30 Days'
                ]
            ]) ?>
        </div>
    </div>

</div>