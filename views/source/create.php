<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Source */

$this->title = 'เพิ่มแหล่งงบ';

?>
<div class="source-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
