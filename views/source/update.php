<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Source */

$this->title = 'แก้ไขที่มาของแหล่งงบ: ' . $model->SOURCE_ID;

?>
<div class="source-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
