<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Source */

$this->title = $model->source_id;

\yii\web\YiiAsset::register($this);
?>
<div class="source-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->source_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->source_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'SOURCE_ID',
            'SOURCE_NAME',
        ],
    ]) ?>

</div>
