<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use kartik\icons\Icon;

/**
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\User $user
 */
?>
<div class="row">
    <div class="col-md-8 offset-2 col-sm-10 offset-1">
        <div class="card border-primary">
            <div class="card-header" style="background-color:cornflowerblue;color:white">
                <b>ข้อมูลผู้ใช้งาน</b>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 align-right">
                        <?= Icon::show('envelope') ?> E-mail :
                    </div>
                    <div class="col-md-9">
                        <?= $form->field($user, 'email')->textInput(['maxlength' => 255])->label(false) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 align-right">
                        <?= Icon::show('user') ?> Username :
                    </div>
                    <div class="col-md-9">
                        <?= $form->field($user, 'username')->textInput(['maxlength' => 255])->label(false) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 align-right">
                        <?= Icon::show('key') ?> Password :
                    </div>
                    <div class="col-md-9">
                        <?= $form->field($user, 'password')->passwordInput()->label(false)->hint('*รหัสผ่านต้องมากกว่า 6 ตัวอักษร') ?>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
<br>